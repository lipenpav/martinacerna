<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Primary Meta Tags -->
    <title>Martina Černá - Pečení s láskou a pro radost</title>
    <meta name="title" content="Martina Černá - Pečení s láskou a pro radost">
    <meta name="description" content="Pečení je mou vášní již od dětských let, kdy jsem v kuchyni pomáhala své mamince a babičce. Tuto vášeň jsem si přenesla i do dospělosti.">

    <!-- Open Graph / Facebook -->
    <meta property="fb:app_id" content="ХХХХХХ97090909"/>
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://martinacerna.ml/">
    <meta property="og:title" content="Martina Černá - Pečení s láskou a pro radost">
    <meta property="og:description" content="Pečení je mou vášní již od dětských let, kdy jsem v kuchyni pomáhala své mamince a babičce. Tuto vášeň jsem si přenesla i do dospělosti.">
    <meta property="og:image" content="http://martinacerna.ml/img/og.jpg">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="http://martinacerna.ml/">
    <meta property="twitter:title" content="Martina Černá - Pečení s láskou a pro radost">
    <meta property="twitter:description" content="Pečení je mou vášní již od dětských let, kdy jsem v kuchyni pomáhala své mamince a babičce. Tuto vášeň jsem si přenesla i do dospělosti.">
    <meta property="twitter:image" content="http://martinacerna.ml/img/og.jpg">


    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/site.webmanifest">
    <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="style/style.css">
    <script type="text/javascript" charset="UTF-8" src="//cdn.cookie-script.com/s/41d57b7876a0d00a36630096f6177be6.js"></script>
    <script src="https://kit.fontawesome.com/6f0d019549.js" crossorigin="anonymous"></script>
    <script src="scripts.js" ></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-8EK05SQ1JF"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

          gtag('config', 'G-8EK05SQ1JF');
    </script>
</head>
<body>

<div class="hero">
    <div class="heroWrap">
        <h1 class="heading">Pečení s <span class="love">láskou</span> a pro radost</h1>
        <img class="personImage" src="img/person5.png" alt="fotka autorky Martiny Černé">
        <img  class="signature" src="img/signature.png" alt="podpis">
        <img class="cap" src="img/cap.png" alt="kuchařská čepice">
        <div class="textPerson">
            <p>Pečení je mou vášní již od dětských let, kdy jsem v kuchyni pomáhala své mamince a babičce. Vášeň jsem si přenesla i do dospělosti, kdy jsem začala více péct pro rodinu a své okolí. Mé vzdělání je úplně mimo tento obor, přesto si myslím, že to není žádnou překážkou. Velmi ráda zkouším nové dorty a zákusky. Každé nové pečení beru jakou výzvu, ke které přistupuji s pokorou.</p>
        </div>
        <a class="scrollButton" href="#work">Ukázka mé práce</a>
        <img class="gloves" src="img/glove.png" alt="rukavice">
    </div>

</div>


<div class="wrapper">
    <div id="work" class="container">

        <h2>Peču již od dětství a postupně se zdokonaluji</h2>

        <div class="categoriesButtons">
            <a class="catBtn cakeButton" href="cakes.html">
                <span>Dorty</span>
                <img src="img/cake.png" alt="Ikonka dortu">
            </a>
            <a class="catBtn muffinButton" href="muffines.html">
                <span>Muffiny</span>
                <img src="img/cupcake.png" alt="Ikonka muffinu">
            </a>
            <a class="catBtn dessertButton activeButton" href="others.html">
                <span>Zákusky</span>
                <img src="img/dessert.png" alt="Ikonka faflí">
            </a>
            <a class="catBtn othersButton" href="others.html">
                <span>Ostatní</span>
                <img src="img/others.png" alt="Ikonka croasantu">
            </a>
        </div>

        <img class="roller" src="img/roller.png" alt="váleček">
        <div class="gallery">


            <?php
            $textsFile = fopen("img/products/zakusky/texty.txt", "r") or die("Unable to open file!");
            $texts = array();
            while (!feof($textsFile)) {
                array_push($texts, fgets($textsFile));
            }

            fclose($textsFile);

            $files = glob('img/products/zakusky/*.{jpg,png,gif}', GLOB_BRACE);
            $counter = 1;
            foreach($files as $file) {
                echo "<div class='gallery-item' onclick='openModal();currentSlide(" . $counter . ")'>";
                echo "<img class='gallery-image' src='" . $file . "' alt='" . $texts[$counter - 1] . "' >";
                echo "<div class='description'>" . $texts[$counter - 1] . "</div>";
                echo "</div>";
                $counter++;
            }
            ?>


        </div>
        <img class="bag" src="img/bag.png" alt="zdobička">
    </div>
</div>

<!-- The Modal/Lightbox -->
<div id="myModal" class="modal" onclick="closeModal()">
    <span class="close cursor" onclick="closeModal()">&times;</span>
    <div class="modal-content" onclick="event.cancelBubble=true;">

        <?php
        $files = glob('img/products/zakusky/*.{jpg,png,gif}', GLOB_BRACE);
        $counter = 1;
        foreach($files as $file) {
            echo "<div class='mySlides'>";
            echo "<div class='numbertext'>" . $counter . " / " . count($files) . "</div>";
            echo "<img src='" . $file . "' alt='" . $texts[$counter - 1] . "'>";
            echo "</div>";
            $counter++;
        }
        ?>


        <!-- Next/previous controls -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>

        <!-- Caption text -->
        <div class="caption-container">
            <p id="caption"></p>
        </div>

    </div>
    <!-- Thumbnail image controls -->
    <div class="thumbnail">

        <?php
        $files = glob('img/products/zakusky/*.{jpg,png,gif}', GLOB_BRACE);
        $counter = 1;
        foreach($files as $file) {
            echo "<div class='column'>";
            echo "<img class='demo' src='" . $file . "' onclick='currentSlide(" . $counter . ");event.cancelBubble=true;' alt='" . $texts[$counter - 1] . "'>";
            echo "</div>";
            $counter++;
        }
        ?>

    </div>
</div>

<footer>
    <a class="email" href="mailto:martinacerna99@gmail.com ">martinacerna99@gmail.com </a>
    <a class="phone" href="tel:+420727869839">+420 727 869 839</a>
    <p> © Martina Černá, Pavel Lipenský</p>
</footer>


</body>
<!-- Messenger Plugin chatu Code 
<div id="fb-root"></div>

 Your Plugin chatu code 
<div id="fb-customer-chat" class="fb-customerchat">
</div>

<script>
    var chatbox = document.getElementById('fb-customer-chat');
    chatbox.setAttribute("page_id", "106453931701042");
    chatbox.setAttribute("attribution", "biz_inbox");

    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v11.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/cs_CZ/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
-->
</html>


